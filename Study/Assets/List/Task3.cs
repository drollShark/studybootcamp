﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace ListLesson
{
    public class Task3 : MonoBehaviour
    {
        List<int> songInplastic = new List<int>();
        void Start()
        {
            for (int i = 1; i < 20 + 1; i++)
            {
                songInplastic.Add(i);
            }
            for (int i = 0; i < 20; i++)
            {
                int j = songInplastic[i];
                int a = Random.Range(0, 20);
                songInplastic[i] = songInplastic[a];
                songInplastic[a] = j;
            }
            foreach (int song in songInplastic)
            {
                Debug.Log(song);
            }
        }
    }
}

