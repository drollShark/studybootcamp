﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace ListLesson
{
    public class Task4 : MonoBehaviour
    {
        List<int> aliens = new List<int>();

        int s = 0;
        void Start()
        {
            for (int i = 0; i < 99; i++)
            {
                aliens.Add(Random.Range(0, 5));
            }

            for (int i = 0; i < aliens.Count - 1; i++)
            {
                for (int j = 0; j < aliens.Count - 1; j++)
                {
                    if (aliens[j] == aliens[j + 1] && aliens[j + 1] <= aliens.Count)
                    {
                        aliens.Remove(aliens[j]);
                        aliens.Remove(aliens[j + 1]);
                    }
                }
            }

            for (int i = 0; i < aliens.Count; i++)
            {                
                s += aliens[i];
            }
                print(s);
        }
    }
}

