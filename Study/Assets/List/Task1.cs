﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace ListLesson 
{
    public class Task1 : MonoBehaviour
    {
       List<int> Num = new List<int>();

        private void Start()
        {
            for (int i = 0; i < 10; i++)
            {
                Num.Add(Random.Range(0, 10));
            }

            for (int i = 0; i < Num.Count; i++)
            {
                if(Num[i] == 3 || Num[i] == 5)
                {
                    Num.RemoveAt(i);
                }
            }

            Num.Add(12);
            for (int i = 0; i < Num.Count; i++)
            {
                print($"Num: {Num[i]}");
            }
        }
    }
}

