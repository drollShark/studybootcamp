﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace ListLesson
{
    public class Task2 : MonoBehaviour
    {
        int carSpeed;

        List<int> carRulesSpeed = new List<int>();

        private void Start()
        {
            for (int i = 0; i <= 100; i++)
            { 
                carSpeed = Random.Range(0, 120);
                if(carSpeed > 100)
                {
                    carRulesSpeed.Add(carSpeed);
                    carSpeed = 0;
                }
                carSpeed = 0;
            }

            for (int i = 0; i < carRulesSpeed.Count; i++)
            {
                Debug.Log($"Скорость - {carRulesSpeed[i]}");
            }
        }
    }
}

