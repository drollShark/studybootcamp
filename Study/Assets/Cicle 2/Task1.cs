﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Cicle
{
    public class Task1 : MonoBehaviour
    {
        int Levels = 0, peopleOnLevels, s;


        private void Start()
        {
            while(Levels < 45)
            {
                peopleOnLevels = Random.Range(3, 9);
                s += peopleOnLevels;
                Levels++;
            }
            Debug.Log($"Кол-во жильцов = {s}");
        }
    }
}
