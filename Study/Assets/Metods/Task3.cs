﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Methods
{
    public class Task3 : MonoBehaviour
    {
        Text text;

        int k = 1, j, i;


        void Start()
        {
            text = GetComponent<Text>();

            for (int i = 1; i <= 10; i++)
            {
                for (int j = 1; j < i + 1; j++)
                {
                    text.text += k++ + " ";
                }

                text.text += "\n";
            }
        }


    }
}

