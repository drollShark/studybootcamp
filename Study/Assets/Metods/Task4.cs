﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Methods
{
    public class Task4 : MonoBehaviour
    {
        Text text;

        // Update is called once per frame
        void Update()
        {
            text = GetComponent<Text>();


            for (int i = 60; i < 100; i++)
            {
                if (i >= 60 && i < 70)
                {
                    text.text += i + "- Удоволетворительно" + "\n";
                }
                else if (i >= 70 & i < 80)
                {
                    text.text += i + "- хорошо" + "\n";
                }
                else if (i > 80 && i <= 100)
                {
                    text.text += i + "- отлично" + "\n";
                }
            }
        }
    }
}

