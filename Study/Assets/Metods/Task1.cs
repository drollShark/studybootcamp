﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace Methods
{
    public class Task1 : MonoBehaviour
    {
        public int n, s = 0;

        public List<int> primeNum = new List<int>();

        void Start()
        {
            for (int i = 0; i < n; i++)
            {
                if (isPrime(i))
                {
                    primeNum.Add(i);
                }
            }

            s = Sum(primeNum);
        }

        bool isPrime(int number)
        {
            if (number > 2)
            {
                for (int i = 2; i < number; i++)
                {
                    if (number % i == 0)
                    {
                        return false;
                    }
                }
            }

            return true;
        }

        int Sum(List<int> numbers)
        {
            int sum = 0;

            for (int i = 0; i < numbers.Count; i++)
            {
                sum += numbers[i];
            }

            return sum;
        }

    }
}

    