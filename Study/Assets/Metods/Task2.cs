﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Methods
{
    public class Task2 : MonoBehaviour
    {
        Text text;

        private void Start()
        {
            text = GetComponent<Text>();

            for (int i = 1; i <= 4; i++)
            {
                for (int j = 0; j < i; j++)
                {
                    text.text += "0";
                }

                text.text += "\n";
            }
        }
    }
}

