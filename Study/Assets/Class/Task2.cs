﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace ClassLesson
{
    public class Task2 : MonoBehaviour
    {
        NPC Elf, Goblin;

        private void Start()
        {
            Elf = new NPC(100);
            Goblin = new NPC(60, 0, 100);

            Elf.Write();
            Goblin.Write();

        }


        class NPC
        {
            public int charisma;
            public int intelligence;
            public int stamina;


            public NPC(int c ) { charisma = c; }

            public NPC(int c, int i, int s) { charisma = c; intelligence = i; stamina = s; }


            public void Write()
            {
                string npcStat = $"Харизма {charisma}, Интеллект {intelligence}, Выносливость {stamina}";

                Debug.Log(npcStat);
            }
        }
    }
}

