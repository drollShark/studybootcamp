﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace ClassLesson
{
    public class Task1 : MonoBehaviour
    {
        Robot robot;

        

        private void Start()
        {
            robot = new Robot();

            print($"Mass = {robot.Mass}, Power = {robot.Power}, Speed = {robot.Speed}");
        }


        class Robot
        {
            static float mass = 8;
            public float Mass => mass;

            static int power = 10;
            public int Power => power;

            static int speed = 0;
            public int Speed => speed;
        }
    }
}

