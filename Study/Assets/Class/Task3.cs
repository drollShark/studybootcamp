﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace ClassLesson
{
    public class Task3 : MonoBehaviour
    {
        Warrior[] warriors = new Warrior[3];


        private void Start()
        {
            Warrior Goblin = new Warrior(40,60, "Goblin");
            warriors[0] = Goblin;
            Warrior Wizzard = new Warrior(20, 60, "Wizzard");
            warriors[1] = Wizzard;
            Warrior Orc = new Warrior(10, 100, "Orc");
            warriors[2] = Orc;
        }


        private void Update()
        {
            for (int i = 0; i < warriors.Length; i++)
            {
                MakeDamage(warriors[i]);
            }
        }

        void MakeDamage(Warrior warrior)
        {
            Warrior target;
            target = warriors[Random.Range(0, warriors.Length)];
            if (target != warrior)
            {
                target.TakeDamage(warrior.Damage);
                Debug.Log($"Воин:{warrior.Name}, ударил {target.Name} и у него осталось {target.HP}");
            }
        }

        class Warrior
        {
            public string Name;

            public int Damage;

            private int hp;
            public int HP => hp;

            public Warrior(int d, int h, string name) { Damage = d; hp = h; Name = name; }

            public void TakeDamage(int damage)
            {
                hp -= damage;
                if(hp <= 0)
                {
                    Debug.Log("Мертв");
                }
            }
        }
    }
}

