﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace ClassLesson
{
    public class Task4 : MonoBehaviour
    {
        public Text text;
        public InputField field;
        Logger logger;

        private void Start()
        {
            logger = new Logger();
            field.text = logger.Message;
        }


        private void Update()
        {
            logger.Message += field.text;
        }


        public void OnMessageDone()
        {
            logger.Message = field.text;
            text.text = logger.Message;
            field.text = "";
        }

        class Logger
        {
            public string wariningProblem = "Message is Empty";
            string Eproblem = "Sorry, message is empty. Please write your message";

            private string message;
            public string Message
            {
                get
                {
                    if (message.Length == 0)
                    {
                        return wariningProblem;
                    }
                    return message;
                }
                set
                {

                    if (value.Length > 0)
                    {
                        message = value;
                    }
                    else
                    {
                        message = Eproblem;
                    }
                }
            }

        }
    }
}
    


