﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace ArrayLesson
{
    public class Task1 : MonoBehaviour
    {
        Text text;
        int[] numbers = new int[10];
        int s, p = 1;

        private void Start()
        {
            text = GetComponent<Text>();
            for (int i = 0; i <= numbers.Length - 1; i++)
            {
                numbers[i] = Random.Range(-7, 9);
            }

            for (int i = 0; i < numbers.Length; i++)
            {
                s += numbers[i];
                p *= numbers[i];
                text.text += numbers[i] + "\n";
            }
            text.text += ($"s = {s}, p = {p}");
        }
    }
}
